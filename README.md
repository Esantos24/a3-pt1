**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

Aquesta pràctica tracta d'introduir un text, fer una simulació de compactació i mostrar-la per pantalla, seguidament mostrar informació de la compactació i una taula amb els diferents caràcters ordenats per repeticions amb les codis ascendentment. Després reintroduir el text compactat i mostrar-lo descompactat.


```
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_TEXT 100
#define MAX_TEXT2 200
#define MAX_TEXT3 100
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 100
#define MAX_DIGITS 4
#define BuidaBuffer while(getchar()!='\n')

int main()
{
    char text[MAX_TEXT+1];
    char text2[MAX_TEXT2+1];
    char text3[MAX_TEXT3+1];
    char compactat[MAX_DIGITS];
    char auxcar;
    char caracter[MAX_CARACTERS+1];
    int repeticions[MAX_REPETICIONS+1];
    int it,it2,it3,ic,i,j,aux,icodi,l;
    char codis[MAX_CODIS][MAX_DIGITS]= {"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000",
                                        "0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100",
                                        "1101","1110","1111"};

    //INICIALITZACIONS
    it=0;
    it2=0;
    it3=0;
    ic=0;
    icodi=0;
    i=0;
    j=0;
    aux=0;
    text[it]='\0';
    text2[it2]='\0';
    auxcar='\0';

    for(it=0;it<=MAX_CARACTERS;it++){
        caracter[it]='\0';
    }

    for(it=0;it<MAX_REPETICIONS;it++){
        repeticions[it]=1;
    }

    it=0;
    ///////////////////

    printf("Introdueix el text:\n       ");
    scanf("%100[^\n]",text);

    l=0;
    //guardar la longitud del text
    while(text[l]!='\0'){
        l++;
    }

    while(text[it]!='\0'){
        ic=0;
        //Seguir buscant mentre no trobi el caracter
        while(text[it]!=caracter[ic] && caracter[ic]!='\0'){
            ic++;
        }
        if(caracter[ic]=='\0'){
            //copiar caracter del text a caracter
            caracter[ic]=text[it];
        }else{
            //incremetnar repeticions
            repeticions[ic]++;
        }
        it++;
    }

    ic=0;
    while(caracter[ic]!='\0'){
        ic++;
    }

    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1]; //metode bombolla
                repeticions[j+1]=aux;
                auxcar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxcar;
            }
        }
    }

    i=0;
    it=0;
    auxcar='\0';

    while(text[it]!='\0'){
        auxcar=text[it];//copiar caracter de text per anar a per el codi
        i=0;
        while(auxcar!=caracter[i]){//buscar codi del caracter que hem agafat
        i++;
        }
        icodi=0;
        if(auxcar==caracter[i]){
            while(codis[i][icodi]!='\0'){
                text2[it2]=codis[i][icodi];//copiar codi al text2(text per al text compactat)
                it2++;icodi++;
            }
        }
        text2[it2]='$';
        it2++;
        it++;
    }

    //trere $ del final
    it2--;
    text2[it2]='\0';

    printf("\nText compactat:\n        %s\n",text2);

    it=0;
    it2=0;

    //Comptar nº bits
    while(text2[it]!='\0'){
        while(text2[it]!='$' && text2[it]!='\0'){
            it2++;it++;
        }
        while(text2[it]=='$'){
            it++;
        }
    }

    printf("\nBits del text: %d*8 = %d bits\n",l,l*8);
    printf("Bits del text compactat: %d bits\n",it2);
    printf("%% de compactacio: %d%%\n\n",100-((it2*100)/(l*8)));

    printf("   caracter \trepeticions \tcodi assignat\n");
    i=0;
    while(i<ic){
        printf("      %c      ->   %d cop/s    ->      %s\n",caracter[i],repeticions[i],codis[i]);
            i++;
    }

    it2=0;

     BuidaBuffer;
    printf("\nIntrodueix el text compactat:\n\t");
    scanf("%100[^\n]",text2);

    while(text2[it2]!='\0'){
        ic=0;
        while(text2[it2]!='$' && text2[it2]!='\0'){
            //copiar codi de text2 a compactat(text compactat)
            compactat[ic]=text2[it2];
            ic++;
            it2++;
        }
        compactat[ic]='\0';
        //saltar $ final
        if(text2[it2]=='$'){
            it2++;
        }
        //Buscar el codi a la llista de codis
        ic=0;
        i=0;
        icodi=0;

        while(codis[i][icodi]!='\0' || compactat[ic]!='\0'){
            if(compactat[ic]!=codis[i][icodi]){
                ic=0;
                icodi=0;
                i++;
            }else{
                icodi++;
                ic++;
            }
        }
        text3[it3]=caracter[i];
        it3++;
        i=0;
    }
    printf("\nEl text descompactat es:\n\t%s\n",text3);

    return 0;
}
```
![](A3-Pt1.png)

